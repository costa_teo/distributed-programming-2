package tests;

import org.junit.Test;

import it.polito.dp2.BIB.BookReader;
import it.polito.dp2.BIB.sol1.BookReaderImpl;

public class BookReaderTest {

	@Test
	public void test() {
		BookReader br = new BookReaderImpl(null, null);
		
		System.out.println("getISBN:    " + br.getISBN());
		System.out.println("getPublisher:" + br.getPublisher());
		System.out.println("getSubtitle: " + br.getSubtitle());
		System.out.println("getTitle:   " + br.getTitle());
		System.out.println("getYear:   " + br.getYear());
		System.out.println("hashCode:  " + br.hashCode());
		System.out.println("getAuthors: " + br.getAuthors().length);
		System.out.println("getCitingItems: " + br.getCitingItems());

	}

}
