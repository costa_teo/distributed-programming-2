package tests;
import org.junit.Test;

import it.polito.dp2.BIB.BibReader;
import it.polito.dp2.BIB.sol1.BibReaderImpl;

public class BibReaderTest {

	@Test
	public void test() {
		BibReader br = new BibReaderImpl(null);
		
		System.out.println("getBook "+ br.getBook(null));
		System.out.println("getItems "+ br.getItems(null, 0, 0));
		System.out.println("getJournal "+ br.getJournal(null));
		System.out.println("getJournals "+ br.getJournals(null));
		
	}

}
