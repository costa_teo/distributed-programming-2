package tests;

import org.junit.Test;

import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.JournalReaderImpl;

public class JournalReaderTest {

	@Test
	public void test() {
		JournalReader jr = new JournalReaderImpl(null, null);
		System.out.println("getISSN  " + jr.getISSN());
		System.out.println("getPublisher " + jr.getPublisher());
		System.out.println("getTitle " + jr.getTitle());
		System.out.println("getIssue  " + jr.getIssue(0, 0));
		System.out.println("getIssues " + jr.getIssues(0, 0));
	}

}
