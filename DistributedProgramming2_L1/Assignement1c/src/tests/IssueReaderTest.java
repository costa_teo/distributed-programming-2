package tests;
import org.junit.Test;

import it.polito.dp2.BIB.sol1.IssueReaderImpl;

public class IssueReaderTest {

	@Test
	public void test() {
		IssueReaderImpl ir = new IssueReaderImpl(null, null, null);
		
		System.out.println("ir.getNumber :" + ir.getNumber());
		System.out.println("ir.getYear :" + ir.getYear());
		System.out.println("ir.getArticles :" + ir.getArticles().size());
		System.out.println("ir.getJournal :" + ir.getJournal());
	}

}
