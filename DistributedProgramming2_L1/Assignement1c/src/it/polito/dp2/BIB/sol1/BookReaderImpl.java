package it.polito.dp2.BIB.sol1;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.BIB.ItemReader;
import it.polito.dp2.BIB.sol1.jaxb.ArticleType;
import it.polito.dp2.BIB.sol1.jaxb.Biblio;
import it.polito.dp2.BIB.sol1.jaxb.BiblioItemType;
import it.polito.dp2.BIB.sol1.jaxb.BookType;

public class BookReaderImpl implements it.polito.dp2.BIB.BookReader {
	
	private BookType book;
	private Biblio biblio;

	public BookReaderImpl(BookType book, Biblio biblio) {
		this.book = book;
		if(this.book == null){
				this.book = new BookType();
		}
		
		this.biblio= biblio;
		if(this.biblio == null){
			this.biblio = new Biblio();
		}
		
	}

	@Override
	public String[] getAuthors() {
		int i = 0;
		String authors[] = new String[this.book.getAuthor().size()];
		for(String s : this.book.getAuthor()) {
			authors[i++] = s;
		}
		return authors;
	}

	@Override
	public Set<ItemReader> getCitingItems() {
		Set<ItemReader> citingItemReaders = new HashSet<ItemReader>();
		for(BiblioItemType item : this.biblio.getArticleOrBook()) {
			for(BigInteger citingId : this.book.getCitedBy()) {
				if(item.getId().equals(citingId)) {
					if(item instanceof ArticleType) {
						citingItemReaders.add(new ArticleReaderImpl((ArticleType) item, this.biblio));
					}else if(item instanceof BookType) {
						citingItemReaders.add(new BookReaderImpl((BookType) item, this.biblio));	
					}
					
					break;
				}
			}
		}
		return citingItemReaders;
	}

	@Override
	public String getSubtitle() {
		return this.book.getSubtitle();
	}

	@Override
	public String getTitle() {
		return this.book.getTitle();
	}

	@Override
	public String getISBN() {
		if(this.book.getISBN() == null){
			return new String();
		}
		return this.book.getISBN();
	}

	@Override
	public String getPublisher() {
		if(this.book.getPublisher() == null){
			return new String();
		}
		return this.book.getPublisher();
	}

	@Override
	public int getYear() {
		if(this.book.getYear() == null){
			return 0;
		}
		return this.book.getYear().getYear();
	}


}
