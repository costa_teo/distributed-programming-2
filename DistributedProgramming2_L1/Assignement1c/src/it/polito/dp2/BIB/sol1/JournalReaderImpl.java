package it.polito.dp2.BIB.sol1;

import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.BIB.IssueReader;
import it.polito.dp2.BIB.sol1.jaxb.Biblio;
import it.polito.dp2.BIB.sol1.jaxb.JournalType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType.Issue;

public class JournalReaderImpl implements it.polito.dp2.BIB.JournalReader {

	private JournalType journal;
	private Biblio biblio;

	public JournalReaderImpl(JournalType journal, Biblio biblio) {
		this.journal = journal;
		if(this.journal == null){
			this.journal = new JournalType();
		}
		
		this.biblio = biblio;
		if(this.biblio == null){
			this.biblio = new Biblio();
		}
	}

	@Override
	public String getISSN() {
		if(this.journal.getISSN() == null){
			return new String();
		}
		return this.journal.getISSN();
	}

	@Override
	/**
	 * Gets a reader for the issue with the given year and number
	 * */
	public IssueReader getIssue(int year, int number) {
		for(Issue issue : this.journal.getIssue()){
			if(issue.getYear() != null && issue.getYear().getYear() == year 
					&& issue.getNumber() != null && issue.getNumber().intValue() == number){
				return new IssueReaderImpl(issue, this.journal, biblio);
			}
		}
		return null;
	}

	@Override
	/***
	 * Gets readers for all the issues available for this journal in the BIB system
	 * whose year is in a given range (between the given "since" and "to" years inclusive;
	 * if "to" is lower than "since", an empty set is returned)
	 */
	public Set<IssueReader>  getIssues(int since, int to) {
		Set<IssueReader> issueReaders = new HashSet<IssueReader>();
		for(Issue issue : this.journal.getIssue()){
			if(issue.getYear().getYear() >= since && issue.getYear().getYear()<= to){
				issueReaders.add(new IssueReaderImpl(issue, this.journal, biblio));
			}
		}
		return issueReaders;
	}

	@Override
	public String getPublisher() {
		if(this.journal.getPublisher() == null){
			return new String();
		}
		return this.journal.getPublisher();
	}

	@Override
	public String getTitle() {
		if(this.journal.getTitle() == null){
			return new String();
		}
		return this.journal.getTitle();
	}

}
