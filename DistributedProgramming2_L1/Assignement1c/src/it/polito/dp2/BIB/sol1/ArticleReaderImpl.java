package it.polito.dp2.BIB.sol1;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.BIB.IssueReader;
import it.polito.dp2.BIB.ItemReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.ArticleType;
import it.polito.dp2.BIB.sol1.jaxb.Biblio;
import it.polito.dp2.BIB.sol1.jaxb.BiblioItemType;
import it.polito.dp2.BIB.sol1.jaxb.BookType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType.Issue;

public class ArticleReaderImpl implements it.polito.dp2.BIB.ArticleReader {

	private ArticleType article;
	private Biblio biblio;

	public ArticleReaderImpl(ArticleType article, Biblio biblio) {
		
		this.article = article;
		if(this.article == null){
			this.article = new ArticleType();
		}
		 
		this.biblio = biblio;
		if(this.biblio == null){
			this.biblio = new Biblio();
		}
	}

	@Override
	public String[] getAuthors() {
		int i = 0;
		String[] authors = new String[this.article.getAuthor().size()];
		for(String s : this.article.getAuthor()) {
			authors[i++] = s; 
		}
		return authors;
	}

	@Override
	public Set<ItemReader> getCitingItems() {
		Set<ItemReader> citingItemReaders = new HashSet<ItemReader>();
		for(BiblioItemType item : this.biblio.getArticleOrBook()) {
			for(BigInteger citingId : this.article.getCitedBy()) {
				if(item.getId().equals(citingId)) {
					if(item instanceof ArticleType) {
						citingItemReaders.add(new ArticleReaderImpl((ArticleType) item, this.biblio));
					}else if(item instanceof BookType) {
						citingItemReaders.add(new BookReaderImpl((BookType) item, this.biblio));	
					}
				}
			}
		}
		
		return citingItemReaders;
	}

	@Override
	public String getSubtitle() {
		return this.article.getSubtitle();
	}

	@Override
	public String getTitle() {
		return this.article.getTitle();
	}

	@Override
	public IssueReader getIssue() {
		for(JournalType journal : this.biblio.getJournal()) {
			for(Issue journalIssue : journal.getIssue() ) {
				if(journalIssue.getId().equals(this.article.getIssue())) {
					return new IssueReaderImpl(journalIssue, journal, this.biblio);
				}
			}
		}
		return null;
	}

	@Override
	public JournalReader getJournal() {
		for(JournalType journal : this.biblio.getJournal()) {
			if(this.article.getJournal().equals(journal.getISSN())) {
				return new JournalReaderImpl(journal, this.biblio);
			}
		}
		return null;
	}

}
