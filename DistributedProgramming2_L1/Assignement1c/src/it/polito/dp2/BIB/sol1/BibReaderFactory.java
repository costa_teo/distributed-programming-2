package it.polito.dp2.BIB.sol1;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;

import org.xml.sax.SAXException;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import it.polito.dp2.BIB.BibReader;
import it.polito.dp2.BIB.BibReaderException;
import it.polito.dp2.BIB.sol1.BibReaderImpl;
import it.polito.dp2.BIB.sol1.jaxb.ArticleType;
import it.polito.dp2.BIB.sol1.jaxb.Biblio;
import it.polito.dp2.BIB.sol1.jaxb.BiblioItemType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType.Issue;

import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;


import javax.xml.validation.SchemaFactory;

/*creation of an instance of BibRader by unmarshalling (with validation) the file
create the implementation of bibReader then start the BibInfo with this implementation and check the output
perform the test (BibTests)*/

public class BibReaderFactory extends it.polito.dp2.BIB.BibReaderFactory {

	@Override
	public BibReader newBibReader() throws BibReaderException {
		
		try {
			JAXBContext jc = JAXBContext.newInstance("it.polito.dp2.BIB.sol1.jaxb");
			Unmarshaller u = jc.createUnmarshaller();
            SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            
            File xsdFile = new File("./xsd/biblio_e.xsd");
            
            if (xsdFile.exists() && xsdFile.isFile()){ //schema exist
            	Schema xsdSchema = sf.newSchema(xsdFile);
            	u.setSchema(xsdSchema);
            	
            	String xmlFileName = System.getProperty("it.polito.dp2.BIB.sol1.BibInfo.file");
            	
            	if(xmlFileName != null){ //check if the fileName is valid
            		File xmlFile = new File(xmlFileName);
                	if(xmlFile.exists() && xmlFile.isFile()){
                		Object biblioObject = u.unmarshal( xmlFile );
                		if(biblioObject instanceof Biblio){
                			Biblio biblio = (Biblio) biblioObject;
                			checkBiblio(biblio);
                			return new BibReaderImpl(biblio);
                			
                		}else{
                			System.out.println("file xml is not valid");
                			throw new BibReaderException();
                		}
                		
             		
                	}else{// no file xml is provided
                		System.out.println("no file xml is provided");
                		throw new BibReaderException();
                	}
                	
            	}else{//if the file name is not valid
            		System.out.println("if the file name is not valid");
            		throw new BibReaderException();
            	}
            		
            }else{ //schema file doesn't exist.
            	System.out.println("schema file doesn't exist.");
            	throw new BibReaderException();
            }
            
            
        } catch( UnmarshalException ue ) {
            System.out.println( "Caught UnmarshalException" );
            throw new BibReaderException();
            
        } catch( JAXBException je ) {
            je.printStackTrace();
            throw new BibReaderException();
        } catch (SAXException e1) {
			e1.printStackTrace();
			throw new BibReaderException();
		} catch (Exception e) {
			throw new BibReaderException();
		}
    }

	private void checkBiblio(Biblio biblio) throws BibReaderException {
		for(BiblioItemType i : biblio.getArticleOrBook()) {
			if(i instanceof ArticleType) {
				ArticleType a = (ArticleType) i;
				boolean good = false;
				for(JournalType j : biblio.getJournal()) {
					if(a.getJournal().equals(j.getISSN())) { //journal of the article
						for(Issue issue : j.getIssue()) {
							if(issue.getId().equals(a.getIssue())){
								good = true;
							}
						}
					}
				}
				if(!good)
					throw new BibReaderException();
			}
		}
		return;
	}
}


