package it.polito.dp2.BIB.sol1;




import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.BIB.ArticleReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.*;
import it.polito.dp2.BIB.sol1.jaxb.JournalType.Issue;

public class IssueReaderImpl implements it.polito.dp2.BIB.IssueReader {


	private Issue issue;
	private Biblio biblio;
	private JournalType journal;
	

	public IssueReaderImpl(Issue issue, JournalType journal, Biblio biblio){
		
		this.issue = issue;
		if(this.issue == null){
			this.issue = new Issue();
		}
		
		this.biblio = biblio;
		if(this.biblio == null){
			this.biblio = new Biblio();
		}
		
		this.journal = journal;
		if(this.journal == null){
			this.journal = new JournalType();
		}
	}

	@Override
	public Set<ArticleReader> getArticles() {
		Set<ArticleReader> articleReaders = new HashSet<ArticleReader>();
		for(BiblioItemType biblioItem: this.biblio.getArticleOrBook()){
			if(biblioItem.getClass().equals(ArticleType.class)){
				ArticleType article = (ArticleType) biblioItem;
				if(article.getIssue().equals(this.issue.getId())){
					articleReaders.add(new ArticleReaderImpl(article, this.biblio));
				}
				
			}
			
		}
		return articleReaders;
	}

	@Override
	public JournalReader getJournal() {
		return new JournalReaderImpl (this.journal, this.biblio);
	}

	@Override
	public int getNumber() {
		if(this.issue.getNumber() == null){
			return 0;
		}
		return this.issue.getNumber().intValue();
		
	}

	@Override
	public int getYear() {
		if(this.issue.getYear() == null){
			return 0;
		}
		return this.issue.getYear().getYear();
	}

}
