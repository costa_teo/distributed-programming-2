package it.polito.dp2.BIB.sol1;

import java.util.HashSet;
import java.util.Set;

import it.polito.dp2.BIB.BookReader;
import it.polito.dp2.BIB.ItemReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.ArticleType;
import it.polito.dp2.BIB.sol1.jaxb.Biblio;
import it.polito.dp2.BIB.sol1.jaxb.BiblioItemType;
import it.polito.dp2.BIB.sol1.jaxb.BookType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType;
import it.polito.dp2.BIB.sol1.jaxb.JournalType.Issue;

public class BibReaderImpl implements it.polito.dp2.BIB.BibReader{

	private Biblio biblio;
	
	public BibReaderImpl(Biblio biblio) {
		this.biblio = biblio;
		if(this.biblio == null){
			this.biblio = new Biblio();
		}
	}

	@Override
	public BookReader getBook(String isbn) {
		if(isbn == null) {
			return null;
		}
		for(BiblioItemType item :  this.biblio.getArticleOrBook()){
			if(item instanceof BookType){
				BookType book = (BookType) item;
				if(book.getISBN().equals(isbn)){
					return new BookReaderImpl(book, this.biblio);
				}
			}
		}
		return null;
	}

	@Override
	/***
	 * Gets readers for all the items available in the BIB system whose title 
	 * contains the specified keyword and whose publication year is between 
	 * the specified values (between the given "since" and "to" years inclusive; 
	 * if "to" is lower than "since", an empty set is returned).
	 */
	public Set<ItemReader> getItems(String keyword, int since, int to){//only book has publicationYear
		Set<ItemReader> itemReaderSet = new HashSet<ItemReader>(); 
		if(keyword==null) {
			keyword = "";
		}
		for(BiblioItemType item : this.biblio.getArticleOrBook()) {
			if(item.getTitle().contains(keyword)) {
				if(item instanceof BookType) {
					BookType book = (BookType) item;
					int publicationYear = book.getYear().getYear();
					if(publicationYear >= since && publicationYear <= to) {
						itemReaderSet.add(new BookReaderImpl(book, this.biblio));
					}
				}else if(item instanceof ArticleType){ 
					ArticleType article = (ArticleType) item;
					String issnJournalArticle = article.getJournal();
					for(JournalType journal : this.biblio.getJournal()) {
						if(issnJournalArticle.equals(journal.getISSN())) {
							for(Issue issue : journal.getIssue()) {
								if(issue.getId().equals(article.getIssue())) {
									int publicationYear = issue.getYear().getYear();
									if(publicationYear >= since && publicationYear <= to) {
										itemReaderSet.add(new ArticleReaderImpl(article, this.biblio));	
									}
								}
							}	
						}
					}
				}
			}
		}
		return itemReaderSet;
	}

	@Override

	public JournalReader getJournal(String issn) {
		if(issn == null) {
			return null;
		}
		for(JournalType journal : this.biblio.getJournal()){
			if(journal.getISSN().equals(issn)){
				return new JournalReaderImpl(journal, this.biblio);
			}
		}
		return null;
	}

	@Override
	/**
	 * getJournals(java.lang.String keyword)
	 * Gets readers for all the journals available in the BIB system whose title 
	 * or publisher contains the specified keyword 
	 */
	public Set<JournalReader> getJournals(String keyword) {
		if(keyword==null){
			keyword = "";
		}
		Set<JournalReader> journalReadersSet = new HashSet<JournalReader>();
		for(JournalType journal : this.biblio.getJournal() ){
			if(journal.getTitle().contains(keyword) || journal.getPublisher().contains(keyword)){
				journalReadersSet.add(new JournalReaderImpl(journal, this.biblio));
			}
		}
		return journalReadersSet;
	}

}
