package it.polito.dp2.BIB.sol3.db;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.NotFoundException;

import it.polito.dp2.BIB.sol3.service.jaxb.Bookshelf;
import it.polito.dp2.BIB.sol3.service.jaxb.Bookshelves;
import it.polito.dp2.BIB.sol3.service.util.ResourseUtils;




public class MyBookshelfDb {

	ConcurrentHashMap<BigInteger, Bookshelf> bookshelvesMap = new ConcurrentHashMap<BigInteger, Bookshelf>();;
	AtomicInteger last = new AtomicInteger(0);;
	DB n4jDb;
	
	private static MyBookshelfDb db = null;
	
	/* better performance to synchronize only the first time the singleton is created */
	public static MyBookshelfDb createMyBookshelDB(DB n4jDb){
		if(db == null){
			synchronized(MyBookshelfDb.class){				
				if(db == null){
					db	= new MyBookshelfDb(n4jDb);
				}
			}
		}
		return db;
	}
	
	private MyBookshelfDb(DB n4jDb){
		this.n4jDb =n4jDb;
	}	
	
	/* synchronized for the counter on all the map because of mix rusults*/
	public synchronized Bookshelves getBookshelves(String keyword) {
		Bookshelves retu = new Bookshelves();
		
		for(Bookshelf b : bookshelvesMap.values()){
			if(b.getName().contains(keyword)){
					b.setNumberOfAccess(b.getNumberOfAccess().add(BigInteger.ONE));
				retu.getBookshelf().add(b);
			}
		}
		return retu;
	}

	/* ok without synchronized the put is thread safe */
	public Bookshelf createBookshelf(Bookshelf bookshelf, ResourseUtils rutil) {
		/*bookshelvesMap.values().forEach(name -> {
			if(name.getName().equals(bookshelf.getName())){
				throw new ClientErrorException(409);
			}
		});*/ // not requested in pdf, name can be duplicated
		
		BigInteger id = BigInteger.valueOf(last.incrementAndGet()); //atomic integer
		rutil.completeBookshelf(bookshelf, id);
		bookshelvesMap.put(id, bookshelf);
		return bookshelf;	
	}

	/* two get can lead to missing updates of the counter, synchronized on the counter operation, getAll and get make conflict */
	public synchronized Bookshelf getBookshelf(BigInteger id) {
		Bookshelf b = bookshelvesMap.get(id);
		if(b != null)							
			b.setNumberOfAccess(b.getNumberOfAccess().add(BigInteger.ONE));				
		else
			throw new NotFoundException("Bookshelf not found");
		
		return b;
	}


	/* ok without synchronized*/
	public BigInteger deleteBookshelf(BigInteger id) {
		if(bookshelvesMap.remove(id) != null){
			return id;
		}else
			throw new NotFoundException("Bookshelf not found");
			
	}

	/* must be synchronized because of the counter  */
	public synchronized List<BigInteger> getBookshelfItems(BigInteger id) {
		Bookshelf b = bookshelvesMap.get(id);
		if(b == null)
			throw new NotFoundException("Bookshelf not found");
		
		b.setNumberOfAccess(b.getNumberOfAccess().add(BigInteger.ONE));
				
		List<BigInteger> itemList = b.getItems();
		if(itemList == null){
			itemList = new ArrayList<BigInteger>();
		}
		
		return itemList;
	}

	
	/* conflict with its self because the list max size is limited to 20 */
	public synchronized void addBookshelfItem(BigInteger id, BigInteger itemId)  {
		Bookshelf b = this.bookshelvesMap.get(id);
		if( b == null ){
			throw new NotFoundException("Bookshelf not found");
		}
		if(b.getItems().size()>=20)
			throw new ClientErrorException(409);

		if(b.getItems().contains(itemId)){
			throw new BadRequestException("already exists");
		}

		try{			
			if(n4jDb.getItem(itemId)==null){
				throw new NotFoundException("Item not found");
			}
		}catch(Exception e){
			throw new NotFoundException();
		}
					
		b.getItems().add(itemId);

	}


	/* conflict because the list inside the bookshelf is not thread safe */
	public synchronized void deleteBookshelfItem(BigInteger id, BigInteger tid) {
		Bookshelf b = bookshelvesMap.get(id); 
		if(b != null){
			if(b.getItems().remove(tid) == false){
				return;
			}
		}else{			
			throw new NotFoundException("Bookshelf not found");
		}
		
		
	}

	/* conflict with because the list inside the bookshelf is not thread safe */
	public synchronized void removeItemFromAllBookshelves(BigInteger id) {
		for(Bookshelf s : this.bookshelvesMap.values()){
			s.getItems().remove(id);
		}
		
	}

}
