package it.polito.dp2.BIB.sol3.db;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

import it.polito.dp2.BIB.sol3.db.DB;
import it.polito.dp2.BIB.sol3.service.jaxb.Bookshelf;
import it.polito.dp2.BIB.sol3.service.jaxb.Bookshelves;
import it.polito.dp2.BIB.sol3.service.jaxb.Item;
import it.polito.dp2.BIB.sol3.service.jaxb.Items;

public class MyBookshelfDb {

	Map<BigInteger, Bookshelf> bookshelvesMap;
	BigInteger last;
	DB n4jDb;
	
	private static MyBookshelfDb db = null;
	
	public static MyBookshelfDb createMyBookshelDB(DB n4jDb){
		if(db == null){
			db	= new MyBookshelfDb(n4jDb);
		}
		return db;
	}
	
	private MyBookshelfDb(DB n4jDb){
		bookshelvesMap = new ConcurrentHashMap<BigInteger, Bookshelf>();
		this.n4jDb =n4jDb;
		last = BigInteger.ZERO;
	}
	
	/* ok */
	public Bookshelves getBookshelves(String keyword) {
		Bookshelves retu = new Bookshelves();
		
		for(Bookshelf b : bookshelvesMap.values()){
			if(b.getName().contains(keyword)){
				retu.getBookshelf().add(b);
			}
		}
		return retu;
	}

	/* ok */
	public synchronized Bookshelf createBookshelf(Bookshelf bookshelf) {
		bookshelvesMap.values().forEach(name -> {
			if(name.getName().equals(bookshelf.getName())){
				System.out.println("Name already exists");
				throw new BadRequestException("Name already exists");
			}
		});
		
		last = last.add(BigInteger.ONE);
		bookshelf.setId(last);
		System.out.println("####### create bookshelf with id = " + last);
		bookshelvesMap.put(last, bookshelf);
		return bookshelf;	
	}

	/* ok */
	public synchronized Bookshelf getBookshelf(BigInteger id) {
		Bookshelf b = bookshelvesMap.get(id);
		if(b != null)
			b.setNumberOfAccess(bookshelvesMap.get(id).getNumberOfAccess().add(BigInteger.ONE));
		else
			throw new NotFoundException("Bookshelf not found");
		
		return bookshelvesMap.get(id);
	}

	/* ok */
	public synchronized Bookshelf updateBookshelf(BigInteger id, String name) {
		if(!bookshelvesMap.containsKey(id))
			throw new NotFoundException("Bookshelf not found");
		
		bookshelvesMap.get(id).setName(name);
		return bookshelvesMap.get(id);
	}

	/* ok */
	public synchronized BigInteger deleteBookshelf(BigInteger id) {
		if(bookshelvesMap.containsKey(id)){
			bookshelvesMap.remove(id);
			return id;
		}else
			throw new NotFoundException("Bookshelf not found");
			
	}

	/* ok */
	public List<BigInteger> getBookshelfItems(BigInteger id) {
		if(!bookshelvesMap.containsKey(id))
			throw new NotFoundException("Bookshelf not found");
		
		bookshelvesMap.get(id).setNumberOfAccess(bookshelvesMap.get(id).getNumberOfAccess().add(BigInteger.ONE));
				
		
		List<BigInteger> itemList = new ArrayList<BigInteger>();
		for(BigInteger tid : bookshelvesMap.get(id).getItems()){
			itemList.add(tid);
		}
		return itemList;
	}

	
	/* ok */
	public synchronized Item addBookshelfItem(BigInteger id, BigInteger itemId) throws Exception {
		Item toAdd = n4jDb.getItem(itemId);
		if(toAdd==null){
			System.out.println("item = " + itemId + " not found");
			throw new NotFoundException("Item not found");
		}
		if(!this.bookshelvesMap.containsKey(id)){
			throw new NotFoundException("Bookshelf not found");
		}
		
		
		if(this.bookshelvesMap.get(id).getItems().contains(itemId)){
			throw new BadRequestException("already exists");
		}
		
		if(this.bookshelvesMap.get(id).getItems().size()>=20)
			throw new BadRequestException("max size 20");

			
		this.bookshelvesMap.get(id).getItems().add(itemId);
		return toAdd;
	}


	/* ok */
	public void deleteBookshelfItem(BigInteger id, BigInteger tid) {
		if(bookshelvesMap.containsKey(id)){
			if(bookshelvesMap.get(id).getItems().remove(tid) == false){
				System.out.println("############# item = " + tid + " not found");
				throw new NotFoundException("Item not found");
			}
		}else{
			System.out.println("############# bookshelf = " + id + " not found");			
			throw new NotFoundException("Bookshelf not found");
		}
		
		
	}

	/* ok */
	public void removeItemFromAllBookshelves(BigInteger id) {
		for(Bookshelf s : this.bookshelvesMap.values()){
			s.getItems().remove(id);
		}
		
	}

}
