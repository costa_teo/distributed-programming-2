package it.polito.dp2.BIB.sol1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;


import it.polito.dp2.BIB.ArticleReader;
import it.polito.dp2.BIB.BibReader;
import it.polito.dp2.BIB.BibReaderException;
import it.polito.dp2.BIB.BibReaderFactory;
import it.polito.dp2.BIB.BookReader;
import it.polito.dp2.BIB.IssueReader;
import it.polito.dp2.BIB.ItemReader;
import it.polito.dp2.BIB.JournalReader;
import it.polito.dp2.BIB.sol1.jaxb.*;
import it.polito.dp2.BIB.sol1.jaxb.JournalType.Issue;


public class BibInfoSerializer {
	private BibReader monitor;

	
	/**
	 * Default constructror
	 * @throws BibReaderException 
	 */
	public BibInfoSerializer() throws BibReaderException {
		BibReaderFactory factory = BibReaderFactory.newInstance();
		monitor = factory.newBibReader();
	}
	
	public BibInfoSerializer(BibReader monitor) {
		super();
		this.monitor = monitor;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BibInfoSerializer wf;
		try {
			
			
			wf = new BibInfoSerializer();
			Biblio b = wf.serializeAll();
			System.out.println("items = " + wf.monitor.getItems(null, 0, 3000).size());
			System.out.println("journal = " + wf.monitor.getJournals(null).size());
	        
			JAXBContext jc = JAXBContext.newInstance("it.polito.dp2.BIB.sol1.jaxb");
			Marshaller m = jc.createMarshaller();
			m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
			m.marshal( b , System.out );
			
		} catch (BibReaderException e) {
			System.err.println("Could not instantiate data generator.");
			e.printStackTrace();
			System.exit(1);
		}
		catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public Biblio serializeAll() {
		Biblio biblio = new Biblio();
		
		for(JournalType journal : this.getJournals()){
			biblio.getJournal().add(journal);
		}
		
		for(BiblioItemType item : this.getItems()){
			biblio.getArticleOrBook().add(item);
		}
		
		return biblio;
	}

	private List<BiblioItemType> getItems() {
		// Get the list of Items
		
		Set<ItemReader> set = monitor.getItems(null, 0, 3000);
		
		List<BiblioItemType> items = new ArrayList<BiblioItemType>();
		
		// For each Item print related data
		for (ItemReader itemReader: set) {
			
			BiblioItemType item;
							
			//if Article
			if (itemReader instanceof ArticleReader) { 
				ArticleReader articleReader = (ArticleReader) itemReader;
				ArticleType article = new ArticleType();
				
				article.setJournal(articleReader.getJournal().getISSN());
				article.setIssue(BigInteger.valueOf(articleReader.getIssue().hashCode()));
				
				item = article;
			
			} else if (itemReader instanceof BookReader) { //if Book
			
			
				BookReader bookReader = (BookReader) itemReader;
				BookType book = new BookType();
				
				//Publisher
				book.setPublisher(bookReader.getPublisher());
				
				//Publication Year
				try {
					GregorianCalendar year = new GregorianCalendar();
					year.set(GregorianCalendar.YEAR, bookReader.getYear());
					XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(year);
					book.setYear(date);
				} catch (DatatypeConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				book.setISBN(bookReader.getISBN());
				
				item = book;
			}else
				continue; //error -> skip to next one
			

			//if it is an article or a book
			//id
			item.setId(BigInteger.valueOf(itemReader.hashCode()));
			
			//title
			item.setTitle(itemReader.getTitle());
			
			//Subtitle
			if (itemReader.getSubtitle()!=null)
				item.setSubtitle(itemReader.getSubtitle());
				
			//Authors
			String[] authorsReader = itemReader.getAuthors();
			for(String authorReader : authorsReader)
				item.getAuthor().add(authorReader);

					
			Set<ItemReader> citingItems = itemReader.getCitingItems();
			for (ItemReader citing: citingItems) {
				item.getCitedBy().add(BigInteger.valueOf(citing.hashCode()));
			}
			
			items.add(item);
		}
		
		
		
		return items;
	}
	
	private List<JournalType> getJournals() {
		// Get the list of journals
		Set<JournalReader> set = monitor.getJournals(null);
		List<JournalType> journals = new ArrayList<JournalType>();
		
		for (JournalReader journalReader:set) {
			JournalType jour = new JournalType();
			
			jour.setTitle(journalReader.getTitle());
			jour.setPublisher(journalReader.getPublisher());
			jour.setISSN(journalReader.getISSN());
			
			
			for (IssueReader issueReader: journalReader.getIssues(0, 3000)) {
				Issue issue = new Issue();
				
				try {
					GregorianCalendar year = new GregorianCalendar();
					year.set(GregorianCalendar.YEAR, issueReader.getYear());
					XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(year);
					issue.setYear(date);
				} catch (DatatypeConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				issue.setNumber(BigInteger.valueOf(issueReader.getNumber()));
				issue.setId(BigInteger.valueOf(issueReader.hashCode()));
				jour.getIssue().add(issue);
			}
			journals.add(jour);
		}	
		return journals;
	}



}
